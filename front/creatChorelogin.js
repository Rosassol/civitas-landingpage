import postChorelogin from './postChorelogin.js'
const inputemail_login = document.querySelector("#email");
const inputsenha_login = document.querySelector("#senha");
const submitbtnl = document.querySelector("#btn_login");
const errorvazio = document.querySelector(".error#errologin");


submitbtnl.addEventListener("click" , async (event) => {
    event.preventDefault();

    
    const emaill = inputemail_login.value;
    const senhal = inputsenha_login.value;
    console.log(emaill,senhal)
 
    if ( emaill === "" || senhal === "" ) {
        errorvazio.style.display = "flex";
    } else {
        errorvazio.style.display = "none";
        await postChorelogin(emaill,senhal);
    }
});