/* Pegar elementos DOM*/
import postChore from './postChore.js';
const inputnome = document.querySelector("#nome");
const inputemail_cad = document.querySelector("#email_cad");
const inputsenha_cad = document.querySelector("#senha_cad");
const inputdata = document.querySelector("#data");
const submitbtn = document.querySelector("#btn_cad");
const errorvazio = document.querySelector(".error#errocadastro");


submitbtn.addEventListener("click" , async (event) => {
    event.preventDefault();

    const nome = inputnome.value;
    const email = inputemail_cad.value;
    const senha = inputsenha_cad.value;
    const data = inputdata.value;
 
    if (nome == "" ||email == "" ||senha == "" ||data == "" ) {
        errorvazio.style.display = "flex";
    } else {
        errorvazio.style.display = "none";
        await postChore(nome,email,senha,data);
    }
});