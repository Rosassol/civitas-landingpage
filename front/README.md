# <strong>Civitas project ( TT 2022.1)

## 📓 Conteúdo e linguagens do projeto

- JavaScript/DOM
- HTML
- CSS
- HTTP protocol
- CRUD - HTTP methods
- Fetch API

<hr>

## 💻 Rodando o projeto

<br>

#### ➡️ Clone esse repositório

```
https://gitlab.com/Rosassol/civitas-landingpage.git
```

#### ➡️  Entre na pasta do projeto

```
civitas-landingpage
```

#### ➡️ Abra o seu terminal (de preferência o do VScode ou do git) e execute o comando para instalar as dependências

```
npm install
```

#### ➡️ Execute o comando para iniciar a API

```
npx json-server --watch db.json
```

#### ➡️ Abra o arquivo `index.html` ou `ladingpage.html` para iniciar a aplicação

<br>

#### ➡️ Para parar a API, basta executar o comando `CTRL + C` no terminal

<hr>

Created by Rayssa Gomes &hearts;