const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Producto = sequelize.define('Producto', {
    price: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    Description:{
        type: DataTypes.STRING,
        allowNull:false
    },

    the_amount: {
        type: DataTypes.STRING,
        allowNull: false
    },
    evaluation: {
        type: DataTypes.STRING,
        allowNull: false
    },
    payment_method: {
            type: DataTypes.STRING,
            allowNull: false
    },
    photograph:{
        type: DataTypes.STRING,
        allowNull:false
    }
});

Producto.associate = function(models) {
    Producto.belongsTo(models.User);
    Producto.belongsTo(models.Store);
};


module.exports = Producto;