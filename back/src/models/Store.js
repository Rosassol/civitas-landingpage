const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Store = sequelize.define('Store', {

    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    email: {
        type: DataTypes.STRING,
        allowNull: false
    },

    phone_number: {
        type: DataTypes.STRING,
        allowNull: false
    },
});
Store.associate = function(models) {
    Store.hasMany(models.Producto);
};

module.exports = Store;