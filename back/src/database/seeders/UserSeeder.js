const User = require("../../models/User");
const faker = require('faker-br');

const seedUser = async function () {
    try {
      await User.sync({ force: true });
      const users = [];
 
      for (let i = 0; i < 10; i++) {
 
       let user = await User.create({
         name: faker.name.firstName(),
         email: faker.internet.email(),
         date_of_birth: faker.date.between(1990, 2004),
         phone_number: faker.phone.phoneNumber(),
         password: faker.internet.password(),
         CPF: faker.br.cpf(),
       })
    }
} catch (err) { console.log(err); }
}

module.exports = seedUser;