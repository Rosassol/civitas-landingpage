const Producto = require("../../models/Producto");
const faker = require('faker-br');

const seedProducto = async function () {
    try {
      await Producto.sync({ force: true });
      const products = [];
 
      for (let i = 0; i < 10; i++) {
 
       let producto = await Producto.create({
         price: faker.commerce.price(),
         description: faker.lorem.text(),
         the_amount: faker.faker.random.number(),
         evaluation: (faker.random.number()% 5 + 1),
         photograph: faker.image.image(),
       })
    }
} catch (err) { console.log(err); }
}

module.exports = seedProducto;