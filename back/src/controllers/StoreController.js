const {Op} = require('sequelize')
const Store = require('../models/Store');

const index = async(req, res) =>{
    try{
        const store = await Store.findAll();
        return res.status(200).json({store});
    } catch(err){
        console.log(err.message)
        return res.status(500).json({err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const store = await Store.findByPk(id);
        return res.status(200).json({store});
    }catch(err){
        return res.status(500).json({err});
    }
};

const create = async(req,res) => {
    try{
          const store = await Store.create(req.body);
          console.log(req.body);
          return res.status(201).json({message: "Loja cadastrada com sucesso!", store: store});
      }catch(err){
          res.status(500).json({error: err});
      }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Store.update(req.body, {where: {id: id}});
        if(updated) {
            const store = await Store.findByPk(id);
            return res.status(200).send(store);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Loja não encontrado");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Store.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Loja deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Loja não encontrado.");
    }
};

module.exports = {
    update,
    destroy,
    create,
    index,
    show,
}