const router = require("express").Router();

const StoreController=require("../controllers/StoreController")
const ProductoController = require("../controllers/ProductoController");
const UserController = require("../controllers/UserController");

router.post("/User", UserController.create);
router.get("/User /:id", UserController.show); 
router.get("/User", UserController.index); 
router.put("/User/:id", UserController.update);
router.delete("/User/:id", UserController.destroy);

router.post("/Producto", ProductoController.create);
router.get("/Producto/:id", ProductoController.show); 
router.get("/Producto", ProductoController.index); 
router.put("/Producto/:id", ProductoController.update);
router.delete("/Producto/:id", ProductoController.destroy);
router.put("/Producto/purchase/:productoId/User/:userId",ProductoController.purchase);
router.put("/Producto/cancelPurchase/:id",ProductoController.cancelPurchase);


router.post("/Store", StoreController.create);
router.get("/Store /:id", StoreController.show); 
router.get("/Store", StoreController.index); 
router.put("/Store/:id", StoreController.update);
router.delete("/Store/:id", StoreController.destroy);

module.exports = router;
